#include "Arduino.h"
#include <SD.h>
#include <Stream.h>


#define FTP_GET_TIMEOUT 60000
#define FTP_PUT_TIMEOUT 60000
#define FTP_DELETE_TIMEOUT 60000
#define HTTP_POST_TIMEOUT 60000
#define HTTP_GET_TIMEOUT 60000
#define MODEM_START_TIMEOUT 60000
#define ENABLE_GPRS_TIMEOUT 10000
#define ENABLE_GPRS_RETRY 5


class sim7000{
public:
	void init(Stream &serial,int pin);
	bool powerON();
	bool powerOFF();
	bool enableGPRS(String apn);
	bool ftpGETOpen(String apn,String ip, String username, String password, String path, String filename);
	bool ftpDelete(String apn,String ip,String username,String password,String path,String filename);
	int ftpGETRead(char* rxBuffer, int length);
	bool ftpIsOpen();
	bool ftpIsError();
	int getFTPWriteMax();
	bool ftpPUTOpen(String apn,String ip, String username, String password, String path, String filename);
	bool ftpPUTWrite(char* txBuffer, int length);
	void ftpPUTClose();
	bool httpPOST(String apn,String url,char* txBuffer,int length,String contentType="application/x-www-form-urlencoded");
	bool httpPOST(String apn,String url,String filename,String contentType="application/x-www-form-urlencoded");
	int  httpGET(String apn,String url,char* rxBuffer);
	void setDebug(bool state);
	void waitTillReady();
    bool getGPRSStatus();
    int  getCSQ();
private:
	void flushReadBuffer();
	String readLine(bool special=false);
	String waitResponse(String response,int timeout,int length=-1);
	bool runCommands(String commandSeq[],String response[],int timeout,int commandCount);
  Stream* MODEM_SERIAL;
  int MODEM_POWER_PIN;
	bool ftpOpenFlag = false;
	bool ftpOpenErrorFlag=false;
	bool callReadyFlag = false;
	bool smsReadyFlag = false;
	bool gprsReadyFlag = false;
	bool ftpReadyFlag =false;
	bool httpReadyFlag = false;
	bool httpErrorFlag = false;
	int ftpWriteMax=0;
  String apn;
	  


};
